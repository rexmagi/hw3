import javax.swing.*;
import java.util.Scanner;

/**
 * Created by trae on 2/22/2016.
 */
public class main {

    public static void main(String[] args){

        Costs c = new Costs();
        Bill b = new Bill(c);
        b.data();
        b.bill();
       b.dialog();

    }
    public static class Costs{
        public double tuition_per;
        public double tech_fee;
        public double g,s,b;
        public double parking_pass;
        Costs(){
            tuition_per=10000;
            tech_fee=500;
            g=1200;
            s=13000;
            b=15000;
            parking_pass=200;

        }
    }

    public static class Bill{
        Costs cost;
        public double tuition_per;
        public double tech_fee;
        public double g,s,b;
        public double parking_pass;
        String name;
        int credits;
        double room;
        String meal;
        double final_bill=0;
        double cmeal;
        Bill (Costs c){
            tuition_per=10000;
            tech_fee=c.tech_fee;
            g=c.g;
            s=c.s;
            b=c.b;
            parking_pass=c.parking_pass;
            cost = c;
        }

        public void data(){
            Scanner eyes = new Scanner(System.in);
            System.out.println("enter name");
            name = eyes.next();
            System.out.println(" enter credits");
            credits = eyes.nextInt();
            System.out.println("enter room amount");
            room = eyes.nextDouble();
            System.out.println("enter meal plan");
            meal = eyes.next();
            if(meal.equalsIgnoreCase("g"))
                cmeal=cost.g;
            if(meal.equalsIgnoreCase("s"))
                cmeal=cost.s;
            if(meal.equalsIgnoreCase("b"))
                cmeal=cost.b;

        }
        public void bill(){
            final_bill += room+tuition_per*credits+tech_fee+parking_pass+cmeal;
        }

        public void dialog(){
            String result = "";
            result+= name+"\n";
            result+= Integer.toString(credits)+"\n";
            result+="$"+Double.toString(tuition_per*credits)+"\n";
            result+="$"+"$"+Double.toString(room+cmeal)+"\n";
            result+="$"+Double.toString(parking_pass)+"\n";
            result+="$"+"$"+Double.toString(tech_fee)+"\n";
            result+="$"+Double.toString(final_bill)+"\n";
            JOptionPane.showMessageDialog(null, result);
        }



    }
}
